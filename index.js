// exercise 1

const firstNumEl = document.getElementById("firstNum");
const secondNumEl = document.getElementById("secondNum");
const thridNumEl = document.getElementById("thirdNum");
const sortedEl = document.getElementById("sorted");

const sort = () => {
  const firstNumVal = +firstNumEl.value;
  const secondNumVal = +secondNumEl.value;
  const thirdNumVal = +thridNumEl.value;

  if (firstNumVal > secondNumVal && firstNumVal > thirdNumVal) {
    if (secondNumVal > thirdNumVal) {
      sortedEl.innerHTML = `${thirdNumVal} ${secondNumVal} ${firstNumVal}`;
    } else {
      sortedEl.innerHTML = `${secondNumVal} ${thirdNumVal} ${firstNumVal}`;
    }
  } else if (secondNumVal > firstNumVal && secondNumVal > thirdNumVal) {
    if (firstNumVal > thirdNumVal) {
      sortedEl.innerHTML = `${thirdNumVal} ${firstNumVal} ${secondNumVal}`;
    } else {
      sortedEl.innerHTML = `${firstNumVal} ${thirdNumVal} ${secondNumVal}`;
    }
  } else {
    if (firstNumVal > secondNumVal) {
      sortedEl.innerHTML = `${secondNumVal} ${firstNumVal} ${thirdNumVal}`;
    } else {
      sortedEl.innerHTML = `${firstNumVal} ${secondNumVal} ${thirdNumVal}`;
    }
  }
};

// exercise 2
const userEl = document.getElementById("user");
const greetEl = document.getElementById("greet");

const greet = () => {
  let userVal = userEl.value;
  if (userVal === "D") {
    greetEl.innerHTML = `👋 Dad`;
  } else if (userVal === "M") {
    greetEl.innerHTML = `👋 Mom`;
  } else if (userVal === "B") {
    greetEl.innerHTML = `👋 Bro`;
  } else {
    greetEl.innerHTML = `👋 Sis`;
  }
};

// exercise 3
const firNumEl = document.getElementById("firNum");
const secNumEl = document.getElementById("secNum");
const thiNumEl = document.getElementById("thiNum");
const typesEl = document.getElementById("types");

let totalOdd = 0;
let totalEven = 0;

const check = () => {
  const firNumVal = +firNumEl.value;
  const secNumVal = +secNumEl.value;
  const thiNumVal = +thiNumEl.value;

  firNumVal % 2 === 0 ? totalEven++ : totalOdd++;
  secNumVal % 2 === 0 ? totalEven++ : totalOdd++;
  thiNumVal % 2 === 0 ? totalEven++ : totalOdd++;
  typesEl.innerHTML = `Odd:${totalOdd} Even:${totalEven}`;
  totalOdd = 0;
  totalEven = 0;
};

// exercise 4
const firstEdgeEl = document.getElementById("firstEdge");
const secondEdgeEl = document.getElementById("secondEdge");
const thirdEdgeEl = document.getElementById("thirdEdge");
const checkerEl = document.getElementById("checker");

const checkFn = () => {
  const firstEdgeVal = +firstEdgeEl.value;
  const secondEdgeVal = +secondEdgeEl.value;
  const thirdEdgeVal = +thirdEdgeEl.value;

  if (firstEdgeVal === thirdEdgeVal && firstEdgeVal === secondEdgeVal) {
    checkerEl.innerHTML = "equilateral triangle";
  } else if (
    firstEdgeVal === secondEdgeVal ||
    firstEdgeVal === thirdEdgeVal ||
    secondEdgeVal === thirdEdgeVal
  ) {
    checkerEl.innerHTML = "isosceles triangle";
  } else if (
    firstEdgeVal === Math.sqrt(secondEdgeVal ** 2 + thirdEdgeVal ** 2) ||
    secondEdgeVal === Math.sqrt(firstEdgeVal ** 2 + thirdEdgeVal ** 2) ||
    thirdEdgeVal === Math.sqrt(secondEdgeVal ** 2 + firstEdgeVal ** 2)
  ) {
    checkerEl.innerHTML = "Right angle triangle";
  } else {
    checkerEl.innerHTML = "another type ";
  }
};
